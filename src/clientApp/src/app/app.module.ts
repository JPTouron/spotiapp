import { HttpClientModule } from '@angular/common/http'; //for httpclient usage within services and components
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// components
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { SearchComponent } from './components/search/search.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';

// services
import { SpotifyService } from './services/spotify.service';

// routing
import { app_routing } from './app.routes';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    app_routing,
    FormsModule
  ],
  providers: [ // globally defined services these are singleton services for all app lifecycle
    SpotifyService 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
