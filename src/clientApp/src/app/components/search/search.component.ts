import { Component, OnInit } from '@angular/core';
import { SpotifyService } from './../../services/spotify.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html'  
})
export class SearchComponent implements OnInit {



  constructor(public service: SpotifyService, private route: ActivatedRoute) { 

    this.route.queryParams.subscribe(params => {
      this.param1 = params['term'];
    });

  }


  param1: string;

  term: string;

  searchArtist() {
    
    if (this.param1)
    this.term= this.param1.length>0 ? this.param1 :this.term;

    this.service.getArtists(this.term).subscribe(resp => {
      console.log(resp);
    });
  }


  ngOnInit() { }


}
