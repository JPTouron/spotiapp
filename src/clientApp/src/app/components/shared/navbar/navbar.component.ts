import { Component, OnInit } from '@angular/core';
import { SpotifyService } from './../../../services/spotify.service';



@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'  
})
export class NavbarComponent implements OnInit {



  constructor(public service: SpotifyService) { }

  term: string;

  searchArtist() {
    this.service.getArtists(this.term).subscribe(resp => {
      console.log(resp);
    });
  }

  ngOnInit() {
  }

}
