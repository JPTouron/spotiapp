export class Artist {
    id: string;
    name: string;
    externalUrl: string;
    genres: string[];
    popularity: number;
    followers: number;
    image: string;
}