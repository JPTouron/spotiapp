import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Artist } from './../model/artist';

//import just the map operator...
import 'rxjs/add/operator/map';
import { forEach } from '@angular/router/src/utils/collection';



@Injectable()
export class SpotifyService {

  constructor(public client: HttpClient) {
    console.log('service is up and ready');

  }


  artistas: any[];

  getArtists(term: string) {
    let url = `https://api.spotify.com/v1/search?q=${term}&type=track,artist&market=US&limit=20&offset=5`;
    let bearer ="BQB7pzLcbY_DFAKSVmelJWqOhieA1uOdWG7GzKxQtAlcIeT2ccxVCJh59fxA3d3-0VAz4NoMNWupSVwUsEs";

    let myHeaders = new HttpHeaders({
      'Authorization':'Bearer ' + bearer
    });

    //fill artists here ussing Artist class later...
    return this.client.get(url, { headers: myHeaders })
      .map((resp: any) => {


        //this.artistas = 

          let items = resp.artists.items; //this.artistas;
let self = this;
this.artistas=new Array();
        items.forEach(function(item){

          let artist  = new Artist();
          artist.id = item.id;
          artist.externalUrl = item.external_urls;
          artist.followers=item.followers.total;
          artist.image= item.images[1];
          artist.name=item.name;
          artist.popularity=item.popularity;
          
          self.artistas.push(artist);
        });

        return resp.artists.items;


      }); //use the map operator to map stuff...

  }


}
